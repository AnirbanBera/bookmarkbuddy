import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BookmarkCard } from  '../model/class/bookmark-card';
import { BookmarkService } from '../service/bookmark.service';
import { HttpClient } from '@angular/common/http';
import { IThumbnail } from '../model/interface/IThumbnail';

@Component({
  selector: 'app-bookmark-card',
  templateUrl: './bookmark-card.component.html',
  styleUrls: ['./bookmark-card.component.scss']
})
export class BookmarkCardComponent implements OnInit {
  bookmarkCards: BookmarkCard[] = [];
  bookmarkThumbnails = {};
  query: string;
  constructor(private bookmarkService: BookmarkService, private changeDetectorRef: ChangeDetectorRef, private http: HttpClient,) {  }

  ngOnInit() {
    this.bookmarkService.getBookmarkCardData().subscribe(
      bookMarkRaw => {
        this.bookmarkService.setBookmarkRaw(bookMarkRaw);
        bookMarkRaw.forEach((item) => {
          this.bookmarkService.flattenNode(item, this.bookmarkCards);              
              });

              //call thumbnail generation api with delay
              for(let i = 0; i < 6; i++){
                this.bookmarkThumbnails[i] = "https://material.angular.io/assets/img/examples/shiba2.jpg";
                setTimeout(() => {
                  this.http.get('https://buddybookmark.herokuapp.com/thumbnail?url=' + this.bookmarkCards[i].Url)
                  .subscribe(
                    (thumbnailJson:IThumbnail) => {
                      this.bookmarkThumbnails[i] = "data:image/png;base64," + thumbnailJson.imgEncoded;    
                      this.changeDetectorRef.detectChanges();      
                    }
                  );
                }, 19000*(i+1));
            }
            //end call thumbnail generation api with delay
          this.changeDetectorRef.detectChanges();
          this.bookmarkService.setBookmarkProcessed(this.bookmarkCards);
        }
    );

    this.bookmarkService.currentSearch.subscribe(query => this.query = query);
  }

}

