import { Component, OnInit } from '@angular/core';
import { BookmarkCard } from './../model/class/bookmark-card';
import { BookmarkService } from './../service/bookmark.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  query: string;
  searchText: string;
  constructor(private bookmarkService: BookmarkService) { }
  ngOnInit() {
    this.bookmarkService.currentSearch.subscribe(query => this.query = query);
  }
  onChange() {
    this.bookmarkService.changeSearchQuery(this.searchText);
    console.log(this.bookmarkService.getBookmarkCardDataById("2"));
  }

}
