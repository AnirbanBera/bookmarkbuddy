import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [MatButtonModule, MatCardModule, MatInputModule, MatIconModule],
  exports: [MatButtonModule, MatCardModule, MatInputModule, MatIconModule],
})
export class MaterialModule {}
