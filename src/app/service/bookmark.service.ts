///<reference path="../../../node_modules/@types/chrome/index.d.ts" />

import { Injectable } from '@angular/core';
import { BookmarkCard } from '../model/class/bookmark-card';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/bindCallback';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import 'rxjs/add/observable/empty';

@Injectable()
export class BookmarkService {
  bookMarkRaw: any;
  bookMarkProcessed: BookmarkCard[];
  private searchText = (new BehaviorSubject<string>(''));
  currentSearch = this.searchText.asObservable();
  
  constructor() { }

  getBookmarkCardData(): Observable<any> {
    if (typeof (this.bookMarkRaw) === "undefined") {
      let getBookmarkTree = Observable.bindCallback(chrome.bookmarks.getTree);
      return (getBookmarkTree());
    } else {
      return Observable.create(this.bookMarkRaw); //need to check whether this is the right way of doing it
    }
  }

  getBookmarkCardDataById(id: string): BookmarkCard[] { // ## need to check whether to return void or data
    let bookMarkProcessedById: BookmarkCard[] = []; // ## need to pass
    if (typeof (this.bookMarkRaw) !== "undefined") {
      this.flattenNode(this.getBookmarkNodeById(id), bookMarkProcessedById);
    }
    return bookMarkProcessedById;
  }

  setBookmarkRaw(data) {
    this.bookMarkRaw = data;
  }

  setBookmarkProcessed(data: BookmarkCard[]) {
    this.bookMarkProcessed = data;
  }

  flattenNode(node, result: BookmarkCard[]) { //can we pass the result type asa generic
    if (node.children) {
      node.children.forEach((child) => {
        if (child.url && child.title) {
          result.push(new BookmarkCard(child.title, child.url, child.dateAdded, child.id, child.index, child.parentId, node.title));
        }
        this.flattenNode(child, result);
      });
    }
  }

  private getBookmarkNodeById(id: string): any {
    let resultNode;
    let findBookmarkNode = function (bookmarkNode): any {
      if (bookmarkNode.id == id) {
        resultNode = bookmarkNode;
      } else {
        if (typeof (bookmarkNode.children) !== "undefined") {
          bookmarkNode.children.find((child) => { findBookmarkNode(child); });
        }
      }
    };
    this.bookMarkRaw.find(findBookmarkNode);
    return (resultNode);
  }

  changeSearchQuery(query: string) {
    this.searchText.next(query);
  }
}
