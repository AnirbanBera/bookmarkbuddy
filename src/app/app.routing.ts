import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
    { path: '', loadChildren: './bookmark-card/bookmark-card.component' },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);