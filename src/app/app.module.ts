import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule } from '@angular/common/http';
import { AppRouting } from './app.routing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BookmarkCardComponent } from './bookmark-card/bookmark-card.component';
import { BookmarkService } from './service/bookmark.service';
import { SearchBoxComponent } from './search-box/search-box.component';
import { FilterPipe } from './pipes/filter.pipe';



@NgModule({
  declarations: [
    AppComponent,
    BookmarkCardComponent,
    SearchBoxComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    AppRouting
  ],
  providers: [BookmarkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
